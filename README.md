Quick project implementing recursive version of Dual-Split Trees
=======================================

Code
-----------
Code is mostly located in main folder

Scene
-----------
Scene settings are hardcoded in scene.cpp file, in Scene::CreateRandom function 

DST and BVH
-----------------
DST and BVH building and traversal implementations are located in DSTree.cpp and bvhTree.cpp

Building
---------------------
Built using Visual Studio 2019 with C++11

Launch
---------------------
After building, .exe file will be located in Binaries folder 

Output
---------------------
After launch, outputs stats on 50 frames in format:

```
<Method> <Plane Hits> <Triangle Hits> <Total Time>
```

\<Total Time\> is calculated in nanoseconds, \<Method\> is DST or BVH
