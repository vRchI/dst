#pragma once
#include "LiteMath.h"
#include "RayTracerEssentials.h"

class Material
{
public:
	virtual Ray Recast(HitRecord& hit) = 0;
	float3 GetColor() { return color; }
protected:
	float3 color;
};

class DiffuseMat : public Material
{
public:
	DiffuseMat(float3 albedo);
	Ray Recast(HitRecord& hit);
};