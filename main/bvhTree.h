#pragma once
#include <vector>
#include "LiteMath.h"
#include "Material.h"
#include "RayTracerEssentials.h"

//Based on github.com/ttsiodras/renderer
float GetArea(float3 low, float3 high);

struct Triangle
{
	std::shared_ptr<Material> mat;
	float3 vertA, vertB, vertC;
	float3 normal;
	float3 lowPoint, highPoint, center;
	int index;
};

class bvhTree;

struct bvhNode
{
	virtual bool Intersect(Ray& ray, HitRecord& hit) const = 0;
	bool IntersectBB(Ray &ray, float minT) const;
	bvhNode(bool isLeaf) : isLeaf(isLeaf) {}
	
	const bool isLeaf;
	int trisNum;
	float3 lowPoint, highPoint;
	bvhTree* treeRoot;
};

struct bvhInner : public bvhNode
{
	bool Intersect(Ray& ray, HitRecord& hit) const;
	bvhInner() : bvhNode(false) { };
	~bvhInner();

	bvhNode *left = nullptr, *right = nullptr;
};

struct bvhLeaf : public bvhNode
{
	bool Intersect(Ray& ray, HitRecord& hit) const;
	bvhLeaf() : bvhNode(true) { };

	std::vector<Triangle> triangles;
};

class bvhTree
{
public:
	HitRecord Intersect(Ray& ray) const;
	bvhNode *root = nullptr;
	~bvhTree();
};

bvhTree* CreateTree(std::vector<float3>& vertices, std::vector<int>& triangles, std::vector<float3>& normals, std::vector<std::shared_ptr<Material>>& mats);
bvhNode* CreateLeaf(bvhTree* root, std::vector<Triangle>& tris, float3 low, float3 high);
bvhNode* CreateTree(std::vector<Triangle>& tris, float3 lowCorner, float3 highCorner, bvhTree* root);
