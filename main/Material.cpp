#include "Material.h"

DiffuseMat::DiffuseMat(float3 albedo)
{
	color = albedo;
}

Ray DiffuseMat::Recast(HitRecord& hit)
{
	float3 tangent, bitangent;
	float coefT, coefB, coefN, rest;

	if (std::abs(hit.normal.z) > 0.1) {
		tangent = hit.normal.cross(float3(0, 1, 0));
	}
	else {
		tangent = hit.normal.cross(float3(0, 0, 1));
	}
	tangent = tangent.normalize();
	bitangent = hit.normal.cross(tangent).normalize();

	coefN = RandomFloat(0, 1);
	rest = 1 - coefN;
	coefB = RandomFloat(-rest, rest);
	coefT = rest - std::abs(coefB);

	return Ray(hit.hitPoint, hit.normal * coefN + tangent * coefT + bitangent * coefB);
}