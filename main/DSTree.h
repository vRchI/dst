#pragma once
#include "RayTracerEssentials.h"
#include "bvhTree.h"

class DSTree;

struct DSTNode
{
	virtual bool Intersect(Ray& ray, HitRecord& hit) const = 0;
	DSTNode(bool isLeaf) : isLeaf(isLeaf) {}
	
	const bool isLeaf;
	float3 lowPoint, highPoint;
	DSTree* treeRoot = nullptr;
	int trisCovered;
};

struct DSTCarving : public DSTNode
{
	enum Mode { DUAL_AXIS, SINGLE_AXIS };
	const Mode splittingMode;
	DSTNode *child = nullptr;

	DSTCarving(Mode mode) : DSTNode(false), splittingMode(mode) {}
	~DSTCarving();
};

struct DSTSingleCarving : public DSTCarving
{
	DSTSingleCarving() : DSTCarving(Mode::SINGLE_AXIS) {}
	DSTSingleCarving(int axis, float3 low, float3 high);
	bool Intersect(Ray& ray, HitRecord& hit) const;

	int axis = -1;
	float valLeft = 0, valRight = 0;
};

struct DSTDualCarving : public DSTCarving
{
	DSTDualCarving() : DSTCarving(Mode::DUAL_AXIS) {}
	DSTDualCarving(int mode, float3 low, float3 high);
	bool Intersect(Ray& ray, HitRecord& hit) const;

	int mode;
	int axisA = -1, axisB = -1;
	float valA = 0, valB = 0;
};

struct DSTSplitting : public DSTNode
{
	DSTSplitting() : DSTNode(false) {}
	bool Intersect(Ray& ray, HitRecord& hit) const;
	~DSTSplitting();

	int axis = -1;
	float valLeft = 0, valRight = 0; //Borders of left and right children
	DSTNode *left = nullptr, *right = nullptr;
};

struct DSTLeaf : public DSTNode
{
	DSTLeaf() : DSTNode(true) {}
	DSTLeaf(bvhLeaf*, DSTree*);
	bool Intersect(Ray& ray, HitRecord& hit) const;

	std::vector<Triangle> triangles;
};

class DSTree
{
public:
	float3 low, high;
	HitRecord Intersect(Ray& ray) const;
	DSTNode* root = nullptr;
	~DSTree();
};

DSTree *CreateDSTree(std::vector<float3>& vertices, std::vector<int>& triangles, std::vector<float3>& normals, std::vector<std::shared_ptr<Material>>& mats);
DSTree *CreateDSTree(bvhTree * tree);
DSTNode *SplitBVHNode(bvhNode*, DSTree *root);