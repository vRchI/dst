#version 330

uniform sampler2D texMain;

in vec2 fragmentTexCoord;

out vec4 fragColor;

void main(void)
{	
	fragColor = texture2D(texMain, fragmentTexCoord);
}