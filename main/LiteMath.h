#pragma once

#include <cmath>

const float pi = 3.14159265358979;

inline float RandomFloat(float min = 0, float max = 1)
{
	return min + (max - min) * ((float)rand() / (RAND_MAX + 1.0));
}

struct float2
{
  float x,y;
};

struct float3
{
	float3():x(0), y(0), z(0){}
	float3(float val) : x(val), y(val), z(val) {}
	float3(const float3& other) : x(other.x), y(other.y), z(other.z) {}
	float3(float a,float b, float c): x(a), y(b), z(c) {}
  
	static float3 RandomUnitVector()
	{
		const float a = RandomFloat(0, 2 * pi);
		const float t = RandomFloat(-pi / 2, pi / 2);
		return float3(std::cos(a) * std::cos(t), std::sin(a) * std::cos(t), std::sin(t));
	}

	//Generates random number in cube (minCoord, minCoord, minCoord) to (maxCoord, maxCoord, maxCoord)
	static float3 RandomInCube(float minCoord, float maxCoord)
	{
		return float3(RandomFloat(minCoord, maxCoord), RandomFloat(minCoord, maxCoord), RandomFloat(minCoord, maxCoord));
	}

	float3 normalize() const {
		return *this / magnitude();
	}

	inline float magnitude() const
	{
		return std::sqrt(x * x + y * y + z * z);
	}

	float3 chooseMin(const float3& other) const
	{
		float3 res;

		res.x = std::fminf(x, other.x);
		res.y = std::fminf(y, other.y);
		res.z = std::fminf(z, other.z);
		return res;
	}

	float3 chooseMax(const float3& other) const
	{
		float3 res;

		res.x = std::fmaxf(x, other.x);
		res.y = std::fmaxf(y, other.y);
		res.z = std::fmaxf(z, other.z);
		return res;
	}

	float dot(const float3& other) const { return x * other.x + y * other.y + z * other.z; }
	float3 cross(const float3& o) const { return float3(y * o.z - z * o.y, z * o.x - x * o.z, x * o.y - y * o.x); }
	float3 invert () const { return float3(1 / x, 1 / y, 1 / z); }

	float3 operator/ (const float val) const { return float3(x / val, y / val, z / val); }
	float3 operator* (const float val) const { return float3(x * val, y * val, z * val); }
	float3 operator+ (const float3& other) const { return float3(x + other.x, y + other.y, z + other.z); }
	float3 operator- (const float3& other) const { return float3(x - other.x, y - other.y, z - other.z); }
	float3 operator* (const float3& other) const { return float3(x * other.x, y * other.y, z * other.z); }
	float3 operator- () const { return float3(-x, -y, -z); }
	float3& operator= (const float3& other) 
	{
		x = other.x;
		y = other.y;
		z = other.z;
		return *this;
	}
	float3& operator= (float3&& other)
	{
		x = other.x;
		y = other.y;
		z = other.z;
		return *this;
	}
	float3& operator+= (const float3& other)
	{
		x += other.x;
		y += other.y;
		z += other.z;
		return *this;
	}
	float3& operator*= (const float3& other)
	{
		x *= other.x;
		y *= other.y;
		z *= other.z;
		return *this;
	}

	float operator[](int ind)
	{
		return (ind == 0) ? x : ((ind == 1) ? y : z);
	}

	float x,y,z;
};

struct float4
{
  float4():x(0), y(0), z(0){}
  float4(float a,float b, float c, float d): x(a), y(b), z(c), w(d) {}

  float x,y,z,w;
};

struct float3x3
{
	float3x3() { identity(); }

	void identity()
	{
		row[0] = float3(1, 0, 0);
		row[1] = float3(0, 1, 0);
		row[2] = float3(0, 0, 1);
	}

	float& M(int x, int y) { return ((float*)row)[y * 3 + x]; }
	float  M(int x, int y) const { return ((float*)row)[y * 3 + x]; }

	float* L() { return (float*)row; }
	const float* L() const { return (float*)row; }

	float3 operator* (const float3& vec) const {
		return float3(row[0].dot(vec), row[1].dot(vec), row[2].dot(vec));
	}

	float3 row[3];
};

struct float4x4
{
  float4x4(){identity();} 

  void identity() 
  { 
    row[0] = float4(1,0,0,0);
    row[1] = float4(0,1,0,0);
    row[2] = float4(0,0,1,0);
    row[3] = float4(0,0,0,1);
  }

  float& M(int x, int y) {return ((float*)row)[y*4+x]; }
  float  M(int x, int y) const {return ((float*)row)[y*4+x]; }
  
  float* L() {return (float*)row;}
  const float* L() const {return (float*)row;}

  float4 row[4]; 
};


