#include <iostream>
#include "HittableMesh.h"
#include "ObjLoader.h"

bool HittableMesh::CheckHit(Ray& ray, HitRecord& hit, float minT)
{
	HitRecord tmp;
	bool isHit = false;
	if (!boundingBox.CheckHit(ray, tmp, minT))
	{
		return false;
	}

	for (int i = 0; i < triangles.size(); i += 3) {
		float3 v0v1 = vertices[triangles[i + 1]] - vertices[triangles[i]];
		float3 v0v2 = vertices[triangles[i + 2]] - vertices[triangles[i]];
		float3 pvec = ray.direction.cross(v0v2);
		float det = v0v1.dot(pvec);
		if (det > 0) {//Culling
			float3 tvec = ray.origin - vertices[triangles[i]];

			float u = tvec.dot(pvec) / det;
			if (u < 0 || u > 1) 
				continue;

			float3 qvec = tvec.cross(v0v1);
			float v = ray.direction.dot(qvec) / det;
			if (v < 0 || u + v > 1) 
				continue;

			float t = v0v2.dot(qvec) / det;

			if (t < minT && t > 0) {
				hit.normal = normals[i / 3];
				hit.isHit = true;
				hit.mat = mat;
				hit.t = t;
				hit.hitPoint = ray.origin + ray.direction * t;
				isHit = true;
			}
		}
	}
	return isHit;
}

HittableMesh HittableMesh::Import(std::string filename, std::shared_ptr<Material> mat, float3 pos, float3 scale)
{
	objl::Loader loader;
	if (!loader.LoadFile(filename)) {
		std::cerr << "Unable to load " << filename << std::endl;
		return HittableMesh();
	}

	HittableMesh res;
	objl::Mesh mesh = loader.LoadedMeshes[0];
	float3 minCoord = float3(INFINITY, INFINITY, INFINITY), maxCoord = float3(-INFINITY, -INFINITY, -INFINITY);

	for (int i = 0; i < mesh.Vertices.size(); i++) {
		auto curVert = mesh.Vertices[i];
		float3 curCoord = float3(curVert.Position.X, curVert.Position.Y, curVert.Position.Z) * scale + pos;
		res.vertices.push_back(curCoord);
		
		if (curCoord.x < minCoord.x) { minCoord.x = curCoord.x; }
		if (curCoord.x > maxCoord.x) { maxCoord.x = curCoord.x; }
		if (curCoord.y < minCoord.y) { minCoord.y = curCoord.y; }
		if (curCoord.y > maxCoord.y) { maxCoord.y = curCoord.y; }
		if (curCoord.z < minCoord.z) { minCoord.z = curCoord.z; }
		if (curCoord.z > maxCoord.z) { maxCoord.z = curCoord.z; }
	}

	for (int i = 0; i < mesh.Indices.size(); i++) {
		auto curInd = mesh.Indices[i];
		res.triangles.push_back(curInd);
		if (i % 3 == 2) {
			float3 sideA = res.vertices[curInd] - res.vertices[res.triangles[i - 1]];
			float3 sideB = res.vertices[curInd] - res.vertices[res.triangles[i - 2]];
			float3 normal = sideA.cross(sideB);

			res.normals.push_back(-normal.normalize());
		}
	}

	res.boundingBox = HittableCube(nullptr, (maxCoord + minCoord) / 2, maxCoord - minCoord);
	res.mat = mat;
	res.position = pos;
	res.scale = scale;
	return res;
}

std::vector<float3>& HittableMesh::GetVertices()
{
	return vertices;
}

std::vector<float3>& HittableMesh::GetNormals()
{
	return normals;
}

std::vector<int>& HittableMesh::GetTriangles()
{
	return triangles;
}

HittableMesh HittableMesh::CopyMove(std::shared_ptr<Material> mat, float3 pos, float3 scl)
{
	HittableMesh res;
	res.position = pos;
	res.scale = scl;
	res.normals = std::vector<float3>(normals.begin(), normals.end());
	res.triangles = std::vector<int>(triangles.begin(), triangles.end());
	res.vertices = std::vector<float3>(vertices.size());
	res.mat = mat;

	float3 scaleDiff = scl * scale.invert();

	for (int i = 0; i < vertices.size(); i++) {
		res.vertices[i] = (vertices[i] - position) * scaleDiff + pos;
	}

	return res;
}
