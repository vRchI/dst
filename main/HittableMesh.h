#pragma once
#include <string>
#include <vector>
#include <map>
#include "hittable.h"
#include "LiteMath.h"

class HittableMesh : public Hittable
{
public:
	bool CheckHit(Ray& ray, HitRecord& hit, float minT);
	static HittableMesh Import(std::string filename, std::shared_ptr<Material> mat, float3 pos = float3(), float3 scale = float3(1, 1, 1));
	std::vector<float3>& GetVertices();
	std::vector<float3>& GetNormals();
	std::vector<int>& GetTriangles();
	HittableMesh CopyMove(std::shared_ptr<Material> mat, float3 pos = float3(), float3 scale = float3(1, 1, 1));

private:
	float3 position;
	float3 scale;

	std::vector<float3> vertices, normals;
	std::vector<int> triangles;
	HittableCube boundingBox;
};
