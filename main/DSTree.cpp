#include "DSTree.h"
#include <functional>
#include <iostream>

HitRecord DSTree::Intersect(Ray& ray) const
{
	HitRecord res;
	res.t = FLT_MAX;
	res.isHit = false;
	res.planeIntersections += 6;

	float3 tmin = (high - ray.origin) * ray.invDir;
	float3 tmax = (low - ray.origin) * ray.invDir;

	float t0, t1;//Start and end of ray intersection

	if (tmin.x > tmax.x)
	{
		t0 = tmax.x;
		t1 = tmin.x;
	}
	else {
		t0 = tmin.x;
		t1 = tmax.x;
	}

	if (tmin.y > tmax.y)
	{
		t0 = std::fmaxf(t0, tmax.y);
		t1 = std::fminf(t1, tmin.y);
	}
	else {
		t0 = std::fmaxf(t0, tmin.y);
		t1 = std::fminf(t1, tmax.y);
	}

	if (tmin.z > tmax.z)
	{
		t0 = std::fmaxf(t0, tmax.z);
		t1 = std::fminf(t1, tmin.z);
	}
	else {
		t0 = std::fmaxf(t0, tmin.z);
		t1 = std::fminf(t1, tmax.z);
	}

	if (t0 < t1 && t1 > 0) {
		res.tMin = std::fmaxf(t0, 0);
		res.tMax = t1;
		root->Intersect(ray, res);
	}

	return res;
}

DSTree::~DSTree()
{
	if (root) {
		delete root;
	}
}

bool DSTSplitting::Intersect(Ray& ray, HitRecord& hit) const
{
	if (ray.marked) {
		std::cout << "Split " << lowPoint.x << " " << lowPoint.y << " " << lowPoint.z << " " << highPoint.x << " " << highPoint.y << " " << highPoint.z << std::endl << std::flush;
	}

	float t1, t2;
	bool needSwap;//t1 intersects the plane with similar directioned normal to the ray
	bool res = false;
	hit.planeIntersections += 2;
	if (axis == 0) {
		t1 = (valLeft - ray.origin.x) * ray.invDir.x;
		t2 = (valRight - ray.origin.x) * ray.invDir.x;
		needSwap = (ray.direction.x < 0);
	} else if (axis == 1) {
		t1 = (valLeft - ray.origin.y) * ray.invDir.y;
		t2 = (valRight - ray.origin.y) * ray.invDir.y;
		needSwap = (ray.direction.y < 0);
	} else {
		t1 = (valLeft - ray.origin.z) * ray.invDir.z;
		t2 = (valRight - ray.origin.z) * ray.invDir.z;
		needSwap = (ray.direction.z < 0);
	}

	if (needSwap) {
		const float tmp = t1;
		t1 = t2;
		t2 = tmp;
	}

	float oldTmin = hit.tMin, oldTmax = hit.tMax;
	bool furtherIntersected = (oldTmax >= t2), closerIntersected = (oldTmin <= t1);

	if (furtherIntersected && closerIntersected) {
		//Both children are intersected
		hit.tMax = std::fminf(hit.tMax, t1);

		//Intersecting closer child
		res |= (needSwap ? right : left)->Intersect(ray, hit);

		hit.tMax = std::fminf(oldTmax, hit.t);
		hit.tMin = std::fmaxf(oldTmin, t2);
		//Intersecting further child if useful
		if (hit.tMin < hit.tMax) {
			res |= (needSwap ? left : right)->Intersect(ray, hit);
		}
	} else {
		if (closerIntersected) {
			hit.tMax = std::fminf(oldTmax, t1);
			res = (needSwap ? right : left)->Intersect(ray, hit);
		}

		if (furtherIntersected) {
			hit.tMin = std::fmaxf(oldTmin, t2);
			res = (needSwap ? left : right)->Intersect(ray, hit);
		}
	}

	return res;
}

DSTSplitting::~DSTSplitting()
{
	if (left) {
		delete left;
	}
	if (right) {
		delete right;
	}
}

DSTCarving::~DSTCarving()
{
	if (child) {
		delete child;
	}
}

DSTree* CreateDSTree(std::vector<float3>& vertices, std::vector<int>& triangles, std::vector<float3>& normals, std::vector<std::shared_ptr<Material>>& mats)
{
	bvhTree* bvh = CreateTree(vertices, triangles, normals, mats);
	
	DSTree* tree = new DSTree();
	tree->root = SplitBVHNode(bvh->root, tree);
	tree->low = bvh->root->lowPoint;
	tree->high = bvh->root->highPoint;
	delete bvh;

	return tree;
}

DSTree* CreateDSTree(bvhTree* bvh)
{
	DSTree* tree = new DSTree();
	tree->root = SplitBVHNode(bvh->root, tree);
	tree->low = bvh->root->lowPoint;
	tree->high = bvh->root->highPoint;
	return tree;
}

struct CarvingTestParameters
{
	float3 targetLow, targetHigh;
	float3 currentLow, currentHigh;
	bool axisCoveredSplit[3][2];
	int coveredSides = 0;
	float nodeScore[4];
	char carveType[3];
	int numTris;
};

struct CarvingTestResults
{
	char carvesCnt = 0;
	float score = FLT_MAX;
	char carveType[3];
	float3 low[3], high[3];
};

//Updates required components of box to target
void SetBox(const float3 &targetLow, const float3 &targetHigh, float3& currentLow, float3& currentHigh, int axis, bool high)
{
	if (axis == 0) {
		if (high) {
			currentHigh.x = targetHigh.x;
		}
		else {
			currentLow.x = targetLow.x;
		}
	} else if (axis == 1) {
		if (high) {
			currentHigh.y = targetHigh.y;
		}
		else {
			currentLow.y = targetLow.y;
		}
	} else {
		if (high) {
			currentHigh.z = targetHigh.z;
		}
		else {
			currentLow.z = targetLow.z;
		}
	}
}

float CalcScore(CarvingTestParameters &params, int depth)
{
	float score = 0;
	for (int i = 0; i < depth; i++) {
		score += params.nodeScore[i] * (params.carveType[i] > 12 ? 0.3f : 0.5f);
	}

	return score + params.numTris * params.nodeScore[depth];
}

bool ChooseCarvingNodes(CarvingTestParameters &params, CarvingTestResults &result, int depth)
{
	//Carving complete, calculating score
	if (params.coveredSides == 6) {
		float score = CalcScore(params, depth - 1);
		if (score < result.score) {
			result.score = score;
			result.carveType[0] = params.carveType[0];
			result.carveType[1] = params.carveType[1];
			result.carveType[2] = params.carveType[2];
			result.carvesCnt = depth - 1;
			return true;
		}
	}

	//3 carves should cover everything
	if (depth > 3) {
		return false;
	}

	bool needSave = false;

	//Total of 15 carve types:
	//12-14 - x, y, z single-axis
	//0-3, 4-7, 8-11 - yz, xz, xy dual-axis
	//0 - both axis from below
	//1 - first axis from below, second - from above
	//2 - first axis from above, second - from below
	//3 - first axis from above, second - from above
	for (int carveType = 0; carveType < 15; carveType++) {
		int axisA, axisB, isHighA, isHighB;
		CarvingTestParameters curParams(params);

		if (carveType > 12) {
			//Single-axis carve
			axisB = axisA = carveType - 12;
			isHighA = 0;
			isHighB = 1;
		}
		else {
			//Dual-axis carve
			axisA = (carveType / 4 + 1) % 3;
			axisB = (carveType / 4 + 2) % 3;
			isHighA = carveType & 1;
			isHighB = (carveType >> 1) & 1;
		}

		int newAxes = (!params.axisCoveredSplit[axisA][isHighA]) + (!params.axisCoveredSplit[axisB][isHighB]);

		if (newAxes == 0) {
			//Carving is useless, both sides are already covered
			continue;
		}

		curParams.axisCoveredSplit[axisA][isHighA] = curParams.axisCoveredSplit[axisB][isHighB] = true;
		//Adding newly covered sides
		curParams.coveredSides += newAxes;

		SetBox(params.targetLow, params.targetHigh, curParams.currentLow, curParams.currentHigh, axisA, isHighA);
		SetBox(params.targetLow, params.targetHigh, curParams.currentLow, curParams.currentHigh, axisB, isHighB);

		curParams.nodeScore[depth] = GetArea(curParams.currentLow, curParams.currentHigh);
		curParams.carveType[depth - 1] = carveType;

		if (ChooseCarvingNodes(curParams, result, depth + 1)) {
			needSave = true;
			result.low[depth - 1] = curParams.currentLow;
			result.high[depth - 1] = curParams.currentHigh;
		}
	}

	return needSave;
}

DSTNode* SplitBVHNode(bvhNode *curNode, DSTree *rootTree)
{
	//Checking if it is leaf
	if (curNode->isLeaf) {
		bvhLeaf* leaf = dynamic_cast<bvhLeaf*>(curNode);
		return new DSTLeaf(leaf, rootTree);
	}
	bvhInner *node = dynamic_cast<bvhInner*>(curNode);

	//Score doesn't include dividing by S and C_I as they are the same for all splits
	CarvingTestResults leftResBest, rightResBest;
	float bestScore = FLT_MAX;
	int bestAxis = -1;
	float valLeft, valRight;

	//Checking all axes for splitting node
	for (int splitAxis = 0; splitAxis < 3; splitAxis++) {
		CarvingTestResults leftResCur, rightResCur;
		CarvingTestParameters leftParam, rightParam;

		leftParam.targetLow = node->left->lowPoint;
		leftParam.targetHigh = node->left->highPoint;
		leftParam.numTris = node->left->trisNum;
		leftParam.currentLow = node->lowPoint;
		leftParam.currentHigh = node->highPoint;

		rightParam.targetLow = node->right->lowPoint;
		rightParam.targetHigh = node->right->highPoint;
		rightParam.numTris = node->right->trisNum;
		rightParam.currentLow = node->lowPoint;
		rightParam.currentHigh = node->highPoint;

		SetBox(leftParam.targetLow, leftParam.targetHigh, leftParam.currentLow, leftParam.currentHigh, splitAxis, true);
		SetBox(rightParam.targetLow, rightParam.targetHigh, rightParam.currentLow, rightParam.currentHigh, splitAxis, false);
		
		//Checking which sides are already covered
		for (int i = 0; i < 3; i++) {
			std::function<bool(const float3&, const float3&)> compFunc;
			if (i == 0) {
				compFunc = [](const float3& a, const float3& b) {return std::abs(a.x - b.x) < 0.001f; };
			}
			else if (i == 1) {
				compFunc = [](const float3& a, const float3& b) {return std::abs(a.y - b.y) < 0.001f; };
			}
			else {
				compFunc = [](const float3& a, const float3& b) {return std::abs(a.z - b.z) < 0.001f; };
			}

			bool val = compFunc(leftParam.targetLow, leftParam.currentLow);
			leftParam.axisCoveredSplit[i][0] = val;
			leftParam.coveredSides += val;
			val = compFunc(leftParam.targetHigh, leftParam.currentHigh);
			leftParam.axisCoveredSplit[i][1] = val;
			leftParam.coveredSides += val;

			val = compFunc(rightParam.targetLow, rightParam.currentLow);
			rightParam.axisCoveredSplit[i][0] = val;
			rightParam.coveredSides += val;
			val = compFunc(rightParam.targetHigh, rightParam.currentHigh);
			rightParam.axisCoveredSplit[i][1] = val;
			rightParam.coveredSides += val;
		}

		leftParam.nodeScore[0] = GetArea(leftParam.currentLow, leftParam.currentHigh);
		rightParam.nodeScore[0] = GetArea(rightParam.currentLow, rightParam.currentHigh);

		ChooseCarvingNodes(leftParam, leftResCur, 1);
		ChooseCarvingNodes(rightParam, rightResCur, 1);

		//Updating best results
		float curScore = leftResCur.score + rightResCur.score;
		if (curScore < bestScore) {
			leftResBest = leftResCur;
			rightResBest = rightResCur;
			bestAxis = splitAxis;
			bestScore = curScore;

			if (splitAxis == 0) {
				valLeft = leftParam.targetHigh.x;
				valRight = rightParam.targetLow.x;
			} else if (splitAxis == 1) {
				valLeft = leftParam.targetHigh.y;
				valRight = rightParam.targetLow.y;
			} else {
				valLeft = leftParam.targetHigh.z;
				valRight = rightParam.targetLow.z;
			}
		}
	}

	//Choosing completed, creating required nodes
	DSTSplitting* splitNode = new DSTSplitting();
	splitNode->axis = bestAxis;
	splitNode->lowPoint = node->lowPoint;
	splitNode->trisCovered = node->trisNum;
	splitNode->highPoint = node->highPoint;
	splitNode->lowPoint = node->lowPoint;
	splitNode->treeRoot = rootTree;

	DSTNode* leftChild = SplitBVHNode(node->left, rootTree);
	DSTNode* rightChild = SplitBVHNode(node->right, rootTree);

	for (int i = leftResBest.carvesCnt; i > 0; i--) {
		DSTCarving *cur;
		if (leftResBest.carveType[i - 1] > 12) {
			cur = new DSTSingleCarving(leftResBest.carveType[i - 1] - 12, leftResBest.low[i - 1], leftResBest.high[i - 1]);
		} else {
			cur = new DSTDualCarving(leftResBest.carveType[i - 1], leftResBest.low[i - 1], leftResBest.high[i - 1]);
		}

		cur->child = leftChild;
		cur->trisCovered = leftChild->trisCovered;
		cur->treeRoot = rootTree;
		leftChild = cur;
	}
	for (int i = rightResBest.carvesCnt; i > 0; i--) {
		DSTCarving* cur;
		if (rightResBest.carveType[i - 1] > 12) {
			cur = new DSTSingleCarving(rightResBest.carveType[i - 1] - 12, rightResBest.low[i - 1], rightResBest.high[i - 1]);
		}
		else {
			cur = new DSTDualCarving(rightResBest.carveType[i - 1], rightResBest.low[i - 1], rightResBest.high[i - 1]);
		}

		cur->child = rightChild;
		cur->trisCovered = rightChild->trisCovered;
		cur->treeRoot = rootTree;
		rightChild = cur;
	}

	splitNode->right = rightChild;
	splitNode->left = leftChild;
	splitNode->valRight = valRight;
	splitNode->valLeft = valLeft;

	return splitNode;
}

DSTLeaf::DSTLeaf(bvhLeaf *leaf, DSTree *root) : triangles(leaf->triangles), DSTNode(true) 
{
	trisCovered = leaf->trisNum;
	lowPoint = leaf->lowPoint;
	highPoint = leaf->highPoint;
	treeRoot = root;
}

bool DSTLeaf::Intersect(Ray& ray, HitRecord& hit) const
{
	bool isHit = false;
	if (ray.marked) {
		std::cout << "Leaf " << lowPoint.x << " " << lowPoint.y << " " << lowPoint.z << " " << highPoint.x << " " << highPoint.y << " " << highPoint.z << std::endl << std::flush;
	}
	hit.triangleIntersections += triangles.size();
	for (auto tri : triangles) {
		float3 v0v1 = tri.vertB - tri.vertA;
		float3 v0v2 = tri.vertC - tri.vertA;
		float3 pvec = ray.direction.cross(v0v2);
		float det = v0v1.dot(pvec);
		if (det > 0) {//Culling
			float3 tvec = ray.origin - tri.vertA;

			float u = tvec.dot(pvec) / det;
			if (u < 0 || u > 1)
				continue;

			float3 qvec = tvec.cross(v0v1);
			float v = ray.direction.dot(qvec) / det;
			if (v < 0 || u + v > 1)
				continue;

			float t = v0v2.dot(qvec) / det;

			if (t < hit.t && t > 0) {
				hit.normal = tri.normal;
				hit.isHit = true;
				hit.mat = tri.mat;
				hit.t = t;
				hit.tMax = t;
				hit.hitPoint = ray.origin + ray.direction * t;
				isHit = true;

				if (ray.marked)
					std::cout << "Hit " << hit.hitPoint.x << " " << hit.hitPoint.y << " " << hit.hitPoint.z << std::endl;
			}
		}
	}

	return isHit;
}

DSTSingleCarving::DSTSingleCarving(int axis, float3 low, float3 high) : axis(axis), DSTCarving(Mode::SINGLE_AXIS)
{
	lowPoint = low;
	highPoint = high;
	if (axis == 0) {
		valLeft = low.x;
		valRight = high.x;
	} else if (axis == 1) {
		valLeft = low.y;
		valRight = high.y;
	} else{
		valLeft = low.z;
		valRight = high.z;
	}
}

bool DSTSingleCarving::Intersect(Ray& ray, HitRecord& hit) const
{
	if (ray.marked) {
		std::cout << "SCarve " << lowPoint.x << " " << lowPoint.y << " " << lowPoint.z << " " << highPoint.x << " " << highPoint.y << " " << highPoint.z << std::endl << std::flush;
	}

	float t1, t2;
	bool needSwap;//t1 intersects the plane with similar directioned normal to the ray

	hit.planeIntersections += 2;
	if (axis == 0) {
		t1 = (valLeft - ray.origin.x) * ray.invDir.x;
		t2 = (valRight - ray.origin.x) * ray.invDir.x;
		needSwap = (ray.direction.x >= 0);
	}
	else if (axis == 1) {
		t1 = (valLeft - ray.origin.y) * ray.invDir.y;
		t2 = (valRight - ray.origin.y) * ray.invDir.y;
		needSwap = (ray.direction.y >= 0);
	}
	else {
		t1 = (valLeft - ray.origin.z) * ray.invDir.z;
		t2 = (valRight - ray.origin.z) * ray.invDir.z;
		needSwap = (ray.direction.z >= 0);
	}

	if (needSwap) {
		const float tmp = t1;
		t1 = t2;
		t2 = tmp;
	}

	if (t1 >= hit.tMin && t2 <= hit.tMax && (t1 < hit.t || t2 < hit.t)) {
		hit.tMax = std::fminf(t1, hit.tMax);
		hit.tMin = std::fmaxf(t2, hit.tMin);
		return child->Intersect(ray, hit);	
	}

	return false;
}

DSTDualCarving::DSTDualCarving(int mode, float3 low, float3 high) : DSTCarving(Mode::DUAL_AXIS), mode(mode)
{
	axisA = (mode / 4 + 1) % 3;
	axisB = (mode / 4 + 2) % 3;
	int isHighA = mode & 1;
	int isHighB = (mode >> 1) & 1;
	lowPoint = low;
	highPoint = high;

	valA = (isHighA ? high : low)[axisA];
	valB = (isHighB ? high : low)[axisB];
}

bool DSTDualCarving::Intersect(Ray& ray, HitRecord& hit) const
{
	if (ray.marked) {
		std::cout << "DCarve " << lowPoint.x << " " << lowPoint.y << " " << lowPoint.z << " " << highPoint.x << " " << highPoint.y << " " << highPoint.z << std::endl << std::flush;
	}

	hit.planeIntersections += 2;
	
	float tmin0, tmin1, tmax0, tmax1;
	float tmin = hit.tMin, tmax = hit.tMax;
	
	tmin0 = (valA - ray.origin[axisA]) * ray.invDir[axisA];
	tmax0 = tmax;
	if ((ray.direction[axisA] > 0) == (mode & 1)) {
		tmax0 = tmin0;
		tmin0 = tmin;
	}

	tmin1 = (valB - ray.origin[axisB]) * ray.invDir[axisB];
	tmax1 = tmax;
	if ((ray.direction[axisB] > 0) == ((mode >> 1) & 1)) {
		tmax1 = tmin1;
		tmin1 = tmin;
	}

	tmin = std::fmaxf(tmin, std::fmaxf(tmin0, tmin1));
	tmax = std::fminf(tmax, std::fminf(tmax0, tmax1));

	if (tmin > tmax || tmin > hit.t) {
		return false;
	}

	hit.tMin = tmin;
	hit.tMax = tmax;

	return child->Intersect(ray, hit);
}
