#pragma once
#include <memory>
#include "RayTracerEssentials.h"
#include "LiteMath.h"
#include "Scene.h"

class RayTracer
{
public:
	RayTracer();
	RayTracer(int width, int height);
	RayTracer &operator=(RayTracer &&other);
	~RayTracer();

	void Step();
	const unsigned char* GetTex();

private:
	Ray CreateRay(int i, int j) const;
	
	double* color_accum;
	unsigned char *tex_ptr;
	int resWidth, resHeight;
	int frameNum = 0;
	const int bounces = 2;

	Scene *bvhScene = nullptr, *dstScene = nullptr, *scene = nullptr;

	//Camera parameters
	const float3 cameraPos = float3(0, 0, -5);
	const float3 clipCenter = float3(0, 0, -4);
	const float clipWidth = 1, clipHeight = 1;
	float3 clipStart;
	float clipStepW, clipStepH;
};

