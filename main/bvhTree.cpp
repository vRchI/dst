#include <queue>
#include <functional>
#include "bvhTree.h"
#include <iostream>

inline float GetArea(float3 low, float3 high)
{
	float3 dif = high - low;
	return dif.x * dif.y + dif.y * dif.z + dif.z * dif.x;
}

bvhTree* CreateTree(std::vector<float3> &vertices, std::vector<int> &triIndices, std::vector<float3> &normals, std::vector<std::shared_ptr<Material>> &mats)
{
	bvhTree* tree = new bvhTree();
	int triCount = triIndices.size() / 3;
	std::vector<Triangle> triangles(triCount);

	float3 globalLow(FLT_MAX), globalHigh(-FLT_MAX);

	//Preparing triangles
	for (int i = 0; i < triCount; i++) {
		triangles[i].vertA = vertices[triIndices[i * 3]];
		triangles[i].vertB = vertices[triIndices[i * 3 + 1]];
		triangles[i].vertC = vertices[triIndices[i * 3 + 2]];

		triangles[i].normal = normals[i];
		triangles[i].index = i;
		triangles[i].mat = mats[i];

		float3 lowCorner(triangles[i].vertA), highCorner(triangles[i].vertA);
		lowCorner = lowCorner.chooseMin(triangles[i].vertB);
		lowCorner = lowCorner.chooseMin(triangles[i].vertC);
		highCorner = highCorner.chooseMax(triangles[i].vertB);
		highCorner = highCorner.chooseMax(triangles[i].vertC);

		triangles[i].lowPoint = lowCorner;
		triangles[i].highPoint = highCorner;
		triangles[i].center = (lowCorner + highCorner) / 2;

		globalLow = globalLow.chooseMin(lowCorner);
		globalHigh = globalHigh.chooseMax(highCorner);
	}

	std::cout << "Building tree of " << triangles.size() << " triangles" << std::endl;
	tree->root = CreateTree(triangles, globalLow, globalHigh, tree);

	return tree;
}

bvhNode* CreateLeaf(bvhTree* root, std::vector<Triangle>& tris, float3 low, float3 high)
{
	bvhLeaf* leaf = new bvhLeaf();
	leaf->treeRoot = root;
	leaf->lowPoint = low;
	leaf->highPoint = high;
	leaf->trisNum = tris.size();

	for (const auto& tri : tris) {
		leaf->triangles.push_back(tri);
	}

	return leaf;
}

bvhNode* CreateTree(std::vector<Triangle> &tris, float3 lowCorner, float3 highCorner, bvhTree *root)
{
	//No need to split further
	if (tris.size() < 8) {
		return CreateLeaf(root, tris, lowCorner, highCorner);
	}

	float minCost = tris.size() * GetArea(lowCorner, highCorner);
	float bestSplit = FLT_MAX;
	int bestAxis = -1;
	float3 bestLeftLow, bestLeftHigh, bestRightLow, bestRightHigh;

	//Testing axes one by one
	for (int axis = 0; axis < 3; axis++)
	{
		float low, split, high;
		std::function<bool(const Triangle&, const Triangle&)> compFunc;

		if (axis == 0) {
			low = lowCorner.x;
			high = highCorner.x;
			compFunc = [](const Triangle& left, const Triangle& right) {return left.center.x < right.center.x; };
		}
		else if (axis == 1) {
			low = lowCorner.y;
			high = highCorner.y;
			compFunc = [](const Triangle& left, const Triangle& right) {return left.center.y < right.center.y; };
		}
		else {
			low = lowCorner.z;
			high = highCorner.z;
			compFunc = [](const Triangle& left, const Triangle& right) {return left.center.z < right.center.z; };
		}

		//If axis difference is too low, it's almost useless to split
		if (high - low < 0.001) continue;

		//A lot of stuff could be reused, though
		int index = 0, size = tris.size();		
		float step = (high - low) / 256.f;
		for (split = low; split < high; split += step) {
			float val;
			float3 leftLowCorn(FLT_MAX), rightLowCorn(FLT_MAX), leftHighCorn(-FLT_MAX), rightHighCorn(-FLT_MAX);
			int leftCnt = 0, rightCnt = 0;

			for (auto tri : tris) {
				if (axis == 0) {
					val = tri.center.x;
				}
				else if (axis == 1) {
					val = tri.center.y;
				}
				else {
					val = tri.center.z;
				}

				if (val < split) {
					leftLowCorn = leftLowCorn.chooseMin(tri.lowPoint);
					leftHighCorn = leftHighCorn.chooseMax(tri.highPoint);
					leftCnt++;
				}
				else {
					rightLowCorn = rightLowCorn.chooseMin(tri.lowPoint);
					rightHighCorn = rightHighCorn.chooseMax(tri.highPoint);
					rightCnt++;
				}
			}

			//Ignoring wasteful partitions
			if (leftCnt < 4 || rightCnt < 4) {
				continue;
			}

			float curCost = leftCnt * GetArea(leftLowCorn, leftHighCorn) + rightCnt * GetArea(rightLowCorn, rightHighCorn);

			if (curCost < minCost) {
				minCost = curCost;
				bestSplit = split;
				bestAxis = axis;

				bestLeftLow = leftLowCorn;
				bestLeftHigh = leftHighCorn;
				bestRightLow = rightLowCorn;
				bestRightHigh = rightHighCorn;
			}
		}	
	}

	//No good candidate found
	if (bestAxis == -1) {
		return CreateLeaf(root, tris, lowCorner, highCorner);
	}

	std::vector<Triangle> leftTris, rightTris;
	float val;
	for (auto tri : tris) {
		if (bestAxis == 0) {
			val = tri.center.x;
		}
		else if (bestAxis == 1) {
			val = tri.center.y;
		}
		else {
			val = tri.center.z;
		}

		if (val < bestSplit) {
			leftTris.push_back(tri);
		}
		else {
			rightTris.push_back(tri);
		}
	}

	if (tris.size() > 1000) {
		std::cout << "Splitting to trees of " << leftTris.size() << " and " << rightTris.size() << " triangles" << std::endl;
	}

	bvhInner *node = new bvhInner();
	node->treeRoot = root;
	node->lowPoint = lowCorner;
	node->highPoint = highCorner;
	node->trisNum = tris.size();
	node->left = CreateTree(leftTris, bestLeftLow, bestLeftHigh, root);
	node->right = CreateTree(rightTris, bestRightLow, bestRightHigh, root);

	return node;
}

bool bvhInner::Intersect(Ray& ray, HitRecord& hit) const
{
	if (ray.marked) {
		std::cout << "BVH " << lowPoint.x << " " << lowPoint.y << " " << lowPoint.z << " " << highPoint.x << " " << highPoint.y << " " << highPoint.z << std::endl;
	}
	bool res = false;
	hit.planeIntersections += 6;
	if (IntersectBB(ray, hit.t)) {
		res |= left->Intersect(ray, hit);
		res |= right->Intersect(ray, hit);
	}
	return res;
}

bvhInner::~bvhInner()
{
	if (left) {
		delete left;
	}

	if (right) {
		delete right;
	}
}

HitRecord bvhTree::Intersect(Ray& ray) const
{
	HitRecord res;
	res.t = FLT_MAX;
	res.isHit = false;
	root->Intersect(ray, res);
	return res;
}

bvhTree::~bvhTree()
{
	if (root) {
		delete root;
	}
}

bool bvhLeaf::Intersect(Ray& ray, HitRecord& hit) const
{
	bool isHit = false;
	hit.planeIntersections += 6; //For BB
	if (!IntersectBB(ray, hit.t))
	{
		return false;
	}
	if (ray.marked) {
		std::cout << "Leaf " << lowPoint.x << " " << lowPoint.y << " " << lowPoint.z << " " << highPoint.x << " " << highPoint.y << " " << highPoint.z << std::endl << std::flush;
	}
	hit.triangleIntersections += triangles.size();
	for (auto tri : triangles) {
		float3 v0v1 = tri.vertB - tri.vertA;
		float3 v0v2 = tri.vertC - tri.vertA;
		float3 pvec = ray.direction.cross(v0v2);
		float det = v0v1.dot(pvec);
		if (det > 0) {//Culling
			float3 tvec = ray.origin - tri.vertA;

			float u = tvec.dot(pvec) / det;
			if (u < 0 || u > 1)
				continue;

			float3 qvec = tvec.cross(v0v1);
			float v = ray.direction.dot(qvec) / det;
			if (v < 0 || u + v > 1)
				continue;

			float t = v0v2.dot(qvec) / det;

			if (t < hit.t && t > 0) {
				hit.normal = tri.normal;
				hit.isHit = true;
				hit.mat = tri.mat;
				hit.t = t;
				hit.hitPoint = ray.origin + ray.direction * t;
				isHit = true;

				if (ray.marked) 
					std::cout << "Hit " << hit.hitPoint.x << " " << hit.hitPoint.y << " " << hit.hitPoint.z << std::endl;
			}
		}
	}

	return isHit;
}

bool bvhNode::IntersectBB(Ray& ray, float minT) const
{
	float3 tmin = (highPoint - ray.origin) * ray.invDir;
	float3 tmax = (lowPoint - ray.origin) * ray.invDir;

	float t0, t1;//Start and end of ray intersection

	if (tmin.x > tmax.x)
	{
		t0 = tmax.x;
		t1 = tmin.x;
	}
	else {
		t0 = tmin.x;
		t1 = tmax.x;
	}

	if (tmin.y > tmax.y)
	{
		t0 = std::fmaxf(t0, tmax.y);
		t1 = std::fminf(t1, tmin.y);
	}
	else {
		t0 = std::fmaxf(t0, tmin.y);
		t1 = std::fminf(t1, tmax.y);
	}

	if (tmin.z > tmax.z)
	{
		t0 = std::fmaxf(t0, tmax.z);
		t1 = std::fminf(t1, tmin.z);
	}
	else {
		t0 = std::fmaxf(t0, tmin.z);
		t1 = std::fminf(t1, tmax.z);
	}

	return t0 < t1 && ((t0 > 0 && t0 < minT) || (t1 > 0 && t1 < minT));
}
