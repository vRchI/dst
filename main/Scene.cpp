#include <ctime>
#include "Scene.h"
#include "HittableMesh.h"

Scene Scene::CreateRandom()
{
	return CreateRandom(std::time(0));
}

Scene Scene::CreateRandom(unsigned int seed)
{
	std::srand(seed);
	Scene scene;
	scene.objects = std::list<std::shared_ptr<Hittable>>();
	
	auto monkeyMesh = std::make_shared<HittableMesh>(HittableMesh::Import("monkey.obj", RandomMaterial(), float3(0, 0, 3), float3(0.8, 0.8, 0.8)));
	scene.objects.emplace_back(std::dynamic_pointer_cast<Hittable>(monkeyMesh));
	
	for (int i = 0; i < 15; i++) {
		auto cloneMesh = std::make_shared<HittableMesh>(monkeyMesh->CopyMove(RandomMaterial(),
			float3::RandomInCube(-1, 3), float3::RandomUnitVector() + float3(0.5)));
		scene.objects.emplace_back(std::dynamic_pointer_cast<Hittable>(cloneMesh));
	}

	auto asteroMesh = std::make_shared<HittableMesh>(HittableMesh::Import("AsterpodNormal.obj", RandomMaterial(), float3(0, 0, 13), float3(2.5)));
	scene.objects.emplace_back(std::dynamic_pointer_cast<Hittable>(asteroMesh));
	//for (int i = 0; i < 2; i++) {
	//	auto cloneMesh = std::make_shared<HittableMesh>(asteroMesh->CopyMove(RandomMaterial(),
	//		float3::RandomInCube(1, 6), float3::RandomUnitVector() + float3(0.5)));
	//	scene.objects.emplace_back(std::dynamic_pointer_cast<Hittable>(cloneMesh));
	//}

	auto archonMesh = std::make_shared<HittableMesh>(HittableMesh::Import("Archon.obj", RandomMaterial(), float3(0, 0, 5), float3(0.3)));
	scene.objects.emplace_back(std::dynamic_pointer_cast<Hittable>(archonMesh));
	for (int i = 0; i < 3; i++) {
		auto cloneMesh = std::make_shared<HittableMesh>(archonMesh->CopyMove(RandomMaterial(),
			float3::RandomInCube(3, 6), float3::RandomUnitVector() + float3(0.2)));
		scene.objects.emplace_back(std::dynamic_pointer_cast<Hittable>(cloneMesh));
	}



	//std::shared_ptr <HittableCube> bgcb = std::make_shared<HittableCube>(RandomMaterial(), float3(0, 0, 6), float3(4, 4, 4));
	//std::shared_ptr <HittableCube> fgcb = std::make_shared<HittableCube>(RandomMaterial(), float3(RandomFloat(), RandomFloat(), -1), float3(0.3, 0.3, 0.3));
	//scene.objects.push_back(std::dynamic_pointer_cast<Hittable>(bgcb));
	//scene.objects.push_back(std::dynamic_pointer_cast<Hittable>(fgcb));
	//
	//for (int i = 0; i < 25; i++)
	//{
	//	int width = i / 5;
	//	int height = i % 5;
	//	std::shared_ptr<Material> mat = RandomMaterial();
	//	std::shared_ptr<HittableSphere> obj = std::make_shared<HittableSphere>(mat, float3(width - 2, height - 2, 0), RandomFloat(0.3, 0.45));
	//	scene.objects.emplace_back(std::dynamic_pointer_cast<Hittable>(obj));
	//}

	return scene;
}

HitRecord Scene::Intersect(Ray ray)
{
	HitRecord res;
	res.t = INFINITY;

	for (auto obj : objects)
	{
		obj->CheckHit(ray, res, res.t);
	}

	return res;
}

std::shared_ptr<Material> Scene::RandomMaterial()
{
	return std::make_shared<DiffuseMat>(float3::RandomInCube(0, 1));
}

BVHScene BVHScene::CreateRandom(int seed)
{
	return BVHScene (Scene::CreateRandom(seed));
}

BVHScene BVHScene::CreateRandom()
{
	return CreateRandom(std::time(0));
}

bvhTree* BVHScene::GetTree()
{
	return tree.get();
}

HitRecord BVHScene::Intersect(Ray ray)
{
	return tree->Intersect(ray);
}

BVHScene::BVHScene(Scene &scene) : Scene(scene)
{
	std::vector<std::shared_ptr<Material>> materials;
	std::vector<float3> vertices, normals;
	std::vector<int> triangles;
	int vertOffset = 0;

	//Combining all meshes
	for (auto obj : objects) {
		auto mesh = std::dynamic_pointer_cast<HittableMesh>(obj);

		//Checking if this is really mesh
		if (mesh) {
			auto mat = mesh->GetMaterial();
			auto curVerts = mesh->GetVertices();
			auto curNorms = mesh->GetNormals();
			auto curTris = mesh->GetTriangles();

			vertices.insert(vertices.end(), curVerts.begin(), curVerts.end());
			normals.insert(normals.end(), curNorms.begin(), curNorms.end());
			for (auto tri : curTris) {
				triangles.push_back(tri + vertOffset);
				materials.push_back(mat);
			}
			vertOffset = vertices.size();
		}
	}

	tree = std::shared_ptr<bvhTree>(CreateTree(vertices, triangles, normals, materials));
}

DSTScene DSTScene::CreateRandom(int seed)
{
	return DSTScene(BVHScene(Scene::CreateRandom(seed)));
}

DSTScene DSTScene::CreateRandom()
{
	return DSTScene::CreateRandom(std::time(0));
}

HitRecord DSTScene::Intersect(Ray ray)
{
	return dsTree->Intersect(ray);
}

DSTScene::DSTScene(Scene&scene) : BVHScene(scene)
{
	dsTree = std::shared_ptr<DSTree>(CreateDSTree(tree.get()));
}

DSTScene::DSTScene(BVHScene *scene)
{
	dsTree = std::shared_ptr<DSTree>(CreateDSTree(scene->GetTree()));
}
