#pragma once
#include "LiteMath.h"
#include "Material.h"
#include <memory>

class Hittable
{
public:
	virtual bool CheckHit(Ray &ray, HitRecord &hit, float minT) = 0;
	std::shared_ptr<Material> GetMaterial();
protected:
	std::shared_ptr<Material> mat;
};

class HittableSphere : public Hittable
{
public:
	HittableSphere(std::shared_ptr<Material> mat, float3 position = float3(), float radius = 1) 
		: position(position), radius(radius) {
		this->mat = mat;
	}
	
	bool CheckHit(Ray& ray, HitRecord& hit, float minT = INFINITY)
	{
		float3 localSpaceDir = ray.origin - position;
		float distance = (localSpaceDir).magnitude();
		if (distance - radius > minT) { //Sphere is too far away
			return false;
		}

		if (ray.direction.dot(localSpaceDir) > 0) { //Sphere is on the opposite side of ray
			return false;
		}

		float b = ray.direction.dot(localSpaceDir);
		float c = localSpaceDir.dot(localSpaceDir) - radius * radius;

		float discriminant = b * b - c; //already divided by 4
		if (discriminant < 0) { //No intersection
			return false;
		}

		discriminant = std::sqrt(discriminant);

		float t = -b - discriminant;
		if (t > 0 && t < minT) {
			hit.hitPoint = ray.origin + ray.direction * t;
			hit.mat = mat;
			hit.normal = (hit.hitPoint - position).normalize();
			hit.t = t;
			hit.isHit = true;

			if (hit.normal.dot(ray.direction) > 0) {
				hit.normal = -hit.normal;
			}
			return true;
		}

		t = -b + discriminant;
		if (t > 0 && t < minT) {
			hit.hitPoint = ray.origin + ray.direction * t;
			hit.mat = mat;
			hit.normal = (hit.hitPoint - position).normalize();
			hit.t = t;
			hit.isHit = true;

			if (hit.normal.dot(ray.direction) > 0) {
				hit.normal = -hit.normal;
			}
			return true;
		}

		return false;
	}

private:
	float3 position;
	float radius;
};

class HittableCube : public Hittable
{
public:
	HittableCube(std::shared_ptr<Material> mat = nullptr, float3 position = float3(), float3 size = float3(1, 1, 1))
		: position(position), size(size), halfSize(size / 2) {
		this->mat = mat;
	}

	bool CheckHit(Ray& ray, HitRecord& hit, float minT = INFINITY)
	{
		float3 tmin = (position - ray.origin - halfSize) * ray.invDir;
		float3 tmax = (position - ray.origin + halfSize) * ray.invDir;
		
		float t0, t1;//Start and end of ray intersection
		float3 normalT0, normalT1;

		if (tmin.x > tmax.x)
		{
			t0 = tmax.x;
			t1 = tmin.x;
			normalT0 = float3(1, 0, 0);
			normalT1 = float3(-1, 0, 0);
		}
		else {
			t0 = tmin.x;
			t1 = tmax.x;
			normalT0 = float3(-1, 0, 0);
			normalT1 = float3(1, 0, 0);
		}

		if (tmin.y > tmax.y)
		{
			if (t0 < tmax.y) {
				t0 = tmax.y;
				normalT0 = float3(0, 1, 0);
			}
			if (t1 > tmin.y) {
				t1 = tmin.y;
				normalT1 = float3(0, -1, 0);
			}
		}
		else {
			if (t0 < tmin.y) {
				t0 = tmin.y;
				normalT0 = float3(0, -1, 0);
			}
			if (t1 > tmax.y) {
				t1 = tmax.y;
				normalT1 = float3(0, 1, 0);
			}
		}

		if (tmin.z > tmax.z)
		{
			if (t0 < tmax.z) {
				t0 = tmax.z;
				normalT0 = float3(0, 0, 1);
			}
			if (t1 > tmin.z) {
				t1 = tmin.z;
				normalT1 = float3(0, 0, -1);
			}
		}
		else {
			if (t0 < tmin.z) {
				t0 = tmin.z;
				normalT0 = float3(0, 0, -1);
			}
			if (t1 > tmax.z) {
				t1 = tmax.z;
				normalT1 = float3(0, 0, 1);
			}
		}

		if (t0 >= t1 || t0 >= minT) {
			return false;
		}

		if (t0 > 0) 
		{
			//we have a good intersection
			hit.hitPoint = ray.origin + ray.direction * t0;
			hit.mat = mat;
			hit.t = t0;
			hit.isHit = true;
			hit.normal = normalT0;

			if (hit.normal.dot(ray.direction) > 0) {
				hit.normal = -hit.normal;
			}
			return true;
		}

		if (t1 > 0 && t1 < minT)
		{
			hit.hitPoint = ray.origin + ray.direction * t1;
			hit.mat = mat;
			hit.t = t1;
			hit.isHit = true;
			hit.normal = normalT1;

			if (hit.normal.dot(ray.direction) > 0) {
				hit.normal = -hit.normal;
			}
			return true;
		}

		return false;
	}

private:
	float3 position;
	float3 size, halfSize;
};