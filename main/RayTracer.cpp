#include "RayTracer.h"
#include <iostream>
#include <chrono>

using steady_clock = std::chrono::steady_clock;

RayTracer::RayTracer() : resWidth(0), resHeight(0), tex_ptr(nullptr) {};

RayTracer::RayTracer(int width, int height) : resWidth(width), resHeight(height)
{
	tex_ptr = new unsigned char[width * height * 4];
	color_accum = new double[width * height * 4];

	//scene = new Scene(Scene::CreateRandom());
	bvhScene = new BVHScene(BVHScene::CreateRandom());
	dstScene = new DSTScene(dynamic_cast<BVHScene *>(bvhScene));

	for (int i = 0; i < width * height * 4; i++) {
		color_accum[i] = 0;
	}

	clipStepW = clipWidth / resWidth;
	clipStepH = clipHeight / resHeight;
	clipStart = clipCenter - float3(clipWidth, clipHeight, 0) / 2;
}

RayTracer &RayTracer::operator=(RayTracer&& other)
{
	resWidth = other.resWidth;
	resHeight = other.resHeight;
	
	tex_ptr = other.tex_ptr;
	color_accum = other.color_accum;
	scene = other.scene;
	bvhScene = other.bvhScene;
	dstScene = other.dstScene;
	other.scene = nullptr;
	other.tex_ptr = nullptr;
	other.color_accum = nullptr;

	clipStart = other.clipStart;
	clipStepH = other.clipStepH;
	clipStepW = other.clipStepW;

	return *this;
}

RayTracer::~RayTracer()
{
	if (tex_ptr) {
		delete[] tex_ptr;
	}

	if (color_accum) {
		delete[] color_accum;
	}

	if (scene) {
		delete scene;
	}
}

const unsigned char* RayTracer::GetTex()
{
	return tex_ptr;
}

Ray RayTracer::CreateRay(int i, int j) const
{
	//Choosing random point inside pixel on clipPlane
	const float3 clipPoint = clipStart + float3((i + RandomFloat()) * clipStepW, (j + RandomFloat()) * clipStepH, 0);

	return Ray(cameraPos, (clipPoint - cameraPos).normalize());
}

void RayTracer::Step()
{
	std::cout << "Frame " << frameNum << std::endl;
	if (++frameNum > 50) {
		exit(0);
	}

	int planeHitsDst = 0, planeHitsBvh = 0;
	int triangleHitsDst = 0, triangleHitsBvh = 0;
	steady_clock::time_point timeDstStart, timeBvhStart;
	steady_clock::time_point timeDstFinish, timeBvhFinish;
	long long totalTimeDst = 0, totalTimeBvh = 0;

	for (int i = 0; i < resHeight; i++) {
		for (int j = 0; j < resWidth; j++) {
			bool singleHit = false;
			Ray currentRay = CreateRay(i, j);
			//if (i == 256 && j == 256) {
			//	currentRay.marked = true;
			//}
			float3 color = float3(1, 1, 1);

			for (int b = 0; b <= bounces; b++)
			{
				timeDstStart = steady_clock::now();
				HitRecord hitDst = dstScene->Intersect(currentRay);
				timeDstFinish = steady_clock::now();
				planeHitsDst += hitDst.planeIntersections;
				triangleHitsDst += hitDst.triangleIntersections;
				totalTimeDst += std::chrono::duration_cast<std::chrono::nanoseconds>(timeDstFinish - timeDstStart).count();

				timeBvhStart = steady_clock::now();
				HitRecord hitBvh = bvhScene->Intersect(currentRay);
				timeBvhFinish = steady_clock::now();
				planeHitsBvh += hitBvh.planeIntersections;
				triangleHitsBvh += hitBvh.triangleIntersections;
				totalTimeBvh += std::chrono::duration_cast<std::chrono::nanoseconds>(timeBvhFinish - timeBvhStart).count();

				if (hitDst.isHit)
				{
					singleHit = true;
					float3 curColor = hitDst.mat->GetColor();
					color *= curColor;

					if (b != bounces) {
						currentRay = hitDst.mat->Recast(hitDst);
					}
				}
				else {
					break;
				}
			}
			
			if (!singleHit) {
				color = float3();
			}

			color_accum[(j * resWidth + i) * 4 + 0] += color.x;
			color_accum[(j * resWidth + i) * 4 + 1] += color.y;
			color_accum[(j * resWidth + i) * 4 + 2] += color.z;

			tex_ptr[(j * resWidth + i) * 4 + 0] = color_accum[(j * resWidth + i) * 4 + 0] * 255 / frameNum;
			tex_ptr[(j * resWidth + i) * 4 + 1] = color_accum[(j * resWidth + i) * 4 + 1] * 255 / frameNum;
			tex_ptr[(j * resWidth + i) * 4 + 2] = color_accum[(j * resWidth + i) * 4 + 2] * 255 / frameNum;
		}
	}

	std::cout << "DST " << planeHitsDst << " " << triangleHitsDst << " " << totalTimeDst << std::endl;
	std::cout << "BVH " << planeHitsBvh << " " << triangleHitsBvh << " " << totalTimeBvh << std::endl;
}