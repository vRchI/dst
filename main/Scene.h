#pragma once
#include <list>
#include "RayTracerEssentials.h"
#include "hittable.h"
#include "LiteMath.h"
#include "bvhTree.h"
#include "DSTree.h"

class Scene
{
public:
	static Scene CreateRandom(unsigned int seed);
	static Scene CreateRandom();

	virtual HitRecord Intersect(Ray ray);

protected:
	static std::shared_ptr<Material> RandomMaterial();
	
	std::list<std::shared_ptr<Hittable>> objects;
};

class BVHScene : public Scene
{
public:
	static BVHScene CreateRandom(int seed);
	static BVHScene CreateRandom();
	bvhTree* GetTree();
	HitRecord Intersect(Ray ray);
	BVHScene(Scene&);
	BVHScene() {}

protected:
	std::shared_ptr<bvhTree> tree = nullptr;
};

class DSTScene : public BVHScene
{
public:
	static DSTScene CreateRandom(int seed);
	static DSTScene CreateRandom();
	HitRecord Intersect(Ray ray);
	DSTScene(Scene &);
	DSTScene(BVHScene *);

protected:
	std::shared_ptr<DSTree> dsTree;
};