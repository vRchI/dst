#pragma once
#include <memory>
#include "LiteMath.h"

class Material;

struct HitRecord
{
	bool isHit = false;
	float3 normal;
	float3 hitPoint;
	std::shared_ptr<Material> mat;
	float t;//Length of intersecting ray
	float tMin, tMax;//Ray boundaries for DST traversal
	int planeIntersections = 0;
	int triangleIntersections = 0;
};

class Ray
{
public:
	float3 origin, direction, invDir;
	Ray(const float3& origin, const float3& dir) : origin(origin), direction(dir.normalize()), invDir(direction.invert()) {};
	bool marked = false;
};